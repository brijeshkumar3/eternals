provider "aws" {
  region = "ap-south-1"
}
locals {
  vpcID = "vpc-a35796c8"
  subnet1 = "subnet-b8752af4"
  subnet2 = "subnet-6900ee02"
  security_group = "sg-0da8b3c0a7751ee47"
  region_name = "ap-south-1"
  aws_account_number = "484010831236"
  ecr_repo_name = "eternal-ecr-cicd-repo"
  app_container_name = "eternal-container"
}
module "eternals-ecs-cluster" {
    source = "./modules/eternals-ecs-cluster"
    cluster_name = "eternals-ecs-cluster"
    team = "ethernals"
}
module "eternals-task-IAM-Role" {
    source = "./modules/IAM/ecs-task-iam-role"
    ecs_iam_role_name = "eternals_iam_role_ecs"
    ecs_iam_role_name_policy = "eternals_iam_role_ecs_policy"
    assume_iam_role_json =  file("iam_role_policy_json/ecs_task_role_defination.json")
    ecs_assume_policy_json =  file("iam_role_policy_json/ecs_task_policy_defination.json")
    codebuild_iam_role_name =  "eternals_iam_role_codebuild"
    codebuild_assume_iam_role_json = file("iam_role_policy_json/code_build_service_role.json")
    codebuild_iam_role_name_policy = "eternals_iam_role_codebuild_policy"
    codebuild_assume_policy_json = file("iam_role_policy_json/code_build_service_role_policy.json")
    codepipeline_iam_role_name = "eternal_codepipeline_iam_role"
    codepipeline_assume_iam_role_json = file("iam_role_policy_json/code_pipeline_service_role.json")
    codepipeline_iam_role_name_policy = "eternal_codepipeline_iam_policy"
    codepipeline_assume_policy_json = file("iam_role_policy_json/code_pipeline_role_policy.json")
}
module "eternals-ecr-registry" {
    source = "./modules/elastic-container-registry"
    ecr-repo-name = local.ecr_repo_name
    ecr-scan-status = true
    ecr-image-tag-mutability = "MUTABLE"
}
module "eternals-task-defination" {
    source = "./modules/eternals-task-defination"
    depends_on = ["module.eternals-task-IAM-Role"]
    container_definitions_path = file("iam_role_policy_json/app_container_definition_json.json")
    task_iam_role = module.eternals-task-IAM-Role.ecs_iam_arn
    AWS_REGION = local.region_name
    app_container_name = local.app_container_name
}
module "eternals-ecs-service" {
    source = "./modules/eternals-ecs-service"
    cluster_name = module.eternals-ecs-cluster.ecs_cluster_name
    taskdefinationarn = module.eternals-task-defination.task-defination-arn
    nw_subnet1 = local.subnet1
    nw_subnet2 = local.subnet2
    ecs_sg = local.security_group
    targetgrouparn = module.eternals-application-LB.targetgrouparn
    containername = "eternal-container"
    containerport = "8080"
} 
module "eternals-application-LB" {
    source = "./modules/eternals-application-loadbalencer"
    nw_subnet1 = local.subnet1
    nw_subnet2 = local.subnet2
    ecs_sg = local.security_group
    vpcid = local.vpcID
}
module "eternals-code-commit" {
    source = "./modules/eternals-code-commit"
    repositoryname = "eternal-cicd-repo"
    branchname =  "master"
}
module "eternals-code-build" {
    source = "./modules/eternals-code-build"
    code_build_project_name = "eternal-cicd-code_build"
    service_role_code_build =  module.eternals-task-IAM-Role.codebuild_iam_arn
    code_commit_repo_name = "eternal-cicd-repo"
    region_name = local.region_name
    ecr_image_name = local.ecr_repo_name
    aws_ecr_repo_name = local.ecr_repo_name
    aws_account_number = local.aws_account_number
    app_container_name = local.app_container_name
}
module "eternals-code-pipeline" {
    source = "./modules/eternals-code-pipeline"
    code_commit_repo_name = "eternal-cicd-repo"
    code_commit_branch_name = "master"
    code_build_project_name =  "eternal-cicd-code_build"
    codepipeline_project_name= "eternals-cicd-code-pipeline"
    cp_service_role_arn = module.eternals-task-IAM-Role.codepipeline_iam_arn
    ecs_cluster_name = module.eternals-ecs-cluster.ecs_cluster_name
    ecs_service_name =  "eteranls-ecs-service"
    ecs_task_def_file_name = "imagedefinitions.json"
    artifacts_s3_bucket_name = "codepipeline-ap-south-1-38008819460"
    region_name = local.region_name
}