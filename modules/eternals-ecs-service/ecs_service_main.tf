resource "aws_ecs_service" "eternal-ecs" {
  name = "eteranls-ecs-service"
  cluster = var.cluster_name
  launch_type = "FARGATE"
  task_definition = var.taskdefinationarn
  desired_count   = 1
  scheduling_strategy = "REPLICA"
  deployment_controller {
    type = "ECS"
  }
  network_configuration {
    security_groups =  [var.ecs_sg]
    subnets   = [ var.nw_subnet1, var.nw_subnet2]
    assign_public_ip = true
  }
  load_balancer {
    target_group_arn = var.targetgrouparn
    container_name   = var.containername
    container_port   = var.containerport
  }
}