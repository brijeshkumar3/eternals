resource "aws_lb" "eternals_alb" {
  name               = "eternals-alb-cicd"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.ecs_sg]
  subnets            = [var.nw_subnet1, var.nw_subnet2]
  enable_deletion_protection = false
  tags = {
    team = "eteranls"
  }
}
resource "local_file" "alb-endpoint" {
    content     = aws_lb.eternals_alb.dns_name
    filename = "${path.root}/details/alb_endpoint.txt"

}
resource "aws_lb_target_group" "albTG" { 
  name     = "eternals-alb-TG"
  port     = 80
  protocol = "HTTP"
  target_type = "ip"
  vpc_id   = var.vpcid
  
  health_check { 
        port = "8080"
        interval = "10"
  }
}
resource "aws_lb_listener" "alb-listener" {
  load_balancer_arn = aws_lb.eternals_alb.arn
  port              = "80"
  protocol          = "HTTP"
  #ssl_policy        = "ELBSecurityPolicy-2016-08"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.albTG.arn 
  }
}
output targetgrouparn {
  value       = aws_lb_target_group.albTG.arn
}
