
resource "aws_s3_bucket" "s3artifacts" {
  bucket = var.artifacts_s3_bucket_name
  acl    = "private"

  tags = {
    Name        = "eternals"
    Environment = "Dev"
  }
}
resource "aws_codepipeline" "eternals-cp" {
  name     = var.codepipeline_project_name
  role_arn = var.cp_service_role_arn

  artifact_store {
    location = aws_s3_bucket.s3artifacts.bucket
    type     = "S3"
  }
  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = ["eternals-docker-source"]

      configuration = {
        RepositoryName = var.code_commit_repo_name
        BranchName     = var.code_commit_branch_name
      }
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["eternals-docker-source"]
      output_artifacts = ["eternals-docker-build"]
      version          = "1"

      configuration = {
        ProjectName = var.code_build_project_name
      }
    }
  }
  stage {
    name = "Deploy"
    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "ECS"
      input_artifacts = ["eternals-docker-build"]
      version         = "1"
      configuration = {
        ClusterName = var.ecs_cluster_name
        ServiceName = var.ecs_service_name
        FileName = var.ecs_task_def_file_name
      }
    }
  }
}