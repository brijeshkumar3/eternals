
variable code_commit_repo_name {
}
variable code_commit_branch_name {
}
variable artifacts_s3_bucket_name {
}
variable code_build_project_name {
}
variable ecs_cluster_name {
}
variable ecs_service_name {
}
variable ecs_task_def_file_name {
}

variable cp_service_role_arn {
}
variable codepipeline_project_name {
}
variable region_name {
}