variable container_definitions_path {
  type        = string
}
variable task_iam_role {
  type        = string
}
variable AWS_REGION {
  default        = "ap-south-1"
}
variable app_container_name {
 type        = string
}