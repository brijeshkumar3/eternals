resource "aws_cloudwatch_log_group" "eternals-log-group" {
  name              = "myloggroup"
}
resource "aws_ecs_task_definition" "service" {
    requires_compatibilities = ["FARGATE"]
    container_definitions = <<DEFINITION
    [
        {
          "name": "${var.app_container_name}",
          "image": "nginx:latest",
          "cpu": 40,
          "memory": 150,
          "essential": true,
          "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
              "awslogs-group" : "${aws_cloudwatch_log_group.eternals-log-group.name}",
              "awslogs-region": "${var.AWS_REGION}",
              "awslogs-stream-prefix": "ecs"
            }
        },
          "portMappings": [
            {
              "containerPort": 8080,
              "hostPort": 8080,
              "protocol": "tcp"
            }
          ]
        }
    ]
DEFINITION
    family = "eternals-ecs-task-defination"
    network_mode = "awsvpc"
    cpu = 256
    memory = 512
    task_role_arn = var.task_iam_role
    execution_role_arn = var.task_iam_role
}
output task-defination-arn {
  value       = aws_ecs_task_definition.service.arn
}