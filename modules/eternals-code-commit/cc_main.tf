resource "aws_codecommit_repository" "eternals-cc" {
  repository_name = var.repositoryname
  description     = "This is the eternals repository"
  default_branch  = var.branchname   
}
resource "null_resource" "example1" {
    provisioner "local-exec" {
        interpreter = ["PowerShell"]
        working_dir = "${path.root}/application-code/"
        command = "git init ; git add . ; git commit -m 'Application code for dev env' ; git push ${aws_codecommit_repository.eternals-cc.clone_url_http} master"
      }
}
  resource "local_file" "codecommithttpurl" {
    content     = aws_codecommit_repository.eternals-cc.clone_url_http
    filename = "${path.root}/details/codecommit_http_url.txt"
}

output name {
  value       = aws_codecommit_repository.eternals-cc.clone_url_http 
}