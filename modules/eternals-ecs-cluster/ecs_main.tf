resource "aws_ecs_cluster" "eternals-ecs-cluster" {
  name = var.cluster_name
  tags = {
    team = var.team
  } 
}
output ecs_cluster_name {
  value       = aws_ecs_cluster.eternals-ecs-cluster.arn
}