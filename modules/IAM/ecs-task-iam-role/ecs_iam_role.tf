resource "aws_iam_role" "ecs_task_iam_role" {
  name = var.ecs_iam_role_name
  path = "/"

  assume_role_policy = var.assume_iam_role_json
}

resource "aws_iam_policy" "ecs_task_iam_role_policy" {
  name        = var.ecs_iam_role_name_policy
  path        = "/"
  description = "task_role"
  policy      = var.ecs_assume_policy_json
  tags = {
    Name = "eternals_iam_policy"
  }
}
resource "aws_iam_policy_attachment" "ecs_iam_role_policy_attach" {
  name       = "policy_attach1"
  roles      = [aws_iam_role.ecs_task_iam_role.name]
  policy_arn = aws_iam_policy.ecs_task_iam_role_policy.arn
}
output ecs_iam_arn {
  value       = aws_iam_role.ecs_task_iam_role.arn
  description = "IAM ARN"
}
