resource "aws_iam_role" "codepipeline_task_iam_role" {
  name = var.codepipeline_iam_role_name
  path = "/"

  assume_role_policy = var.codepipeline_assume_iam_role_json
}

resource "aws_iam_policy" "codepipeline_task_iam_role_policy" {
  name        = var.codepipeline_iam_role_name_policy
  path        = "/"
  description = "task_role"
  policy      = var.codepipeline_assume_policy_json
  tags = {
    Name = "eternals_iam_policy"
  }
}
resource "aws_iam_policy_attachment" "codepipeline_iam_role_policy_attach" {
  name       = "policy_attach"
  roles      = [aws_iam_role.codepipeline_task_iam_role.name]
  policy_arn = aws_iam_policy.codepipeline_task_iam_role_policy.arn
}
output codepipeline_iam_arn {
  value       = aws_iam_role.codepipeline_task_iam_role.arn
  description = "IAM ARN"
}
