resource "aws_iam_role" "codebuild_task_iam_role" {
  name = var.codebuild_iam_role_name
  path = "/"

  assume_role_policy = var.codebuild_assume_iam_role_json
}

resource "aws_iam_policy" "codebuild_task_iam_role_policy" {
  name        = var.codebuild_iam_role_name_policy
  path        = "/"
  description = "task_role"
  policy      = var.codebuild_assume_policy_json
  tags = {
    Name = "eternals_iam_policy"
  }
}
resource "aws_iam_policy_attachment" "codebuild_iam_role_policy_attach" {
  name       = "policy_attach"
  roles      = [aws_iam_role.codebuild_task_iam_role.name]
  policy_arn = aws_iam_policy.codebuild_task_iam_role_policy.arn
}
output codebuild_iam_arn {
  value       = aws_iam_role.codebuild_task_iam_role.arn
  description = "IAM ARN"
}
