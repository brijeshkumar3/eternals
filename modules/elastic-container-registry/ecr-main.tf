resource "aws_ecr_repository" "eternals-ecr-repo" {
  name                 = var.ecr-repo-name
  image_tag_mutability = var.ecr-image-tag-mutability

  image_scanning_configuration {
    scan_on_push = var.ecr-scan-status
  }
}