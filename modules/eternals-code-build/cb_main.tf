resource "aws_codebuild_project" "eternals-cb" {
    name = var.code_build_project_name
    description   = "PoC-Eternals CodeBuild from terraform"
    build_timeout = "30"
    service_role = var.service_role_code_build
    environment {
        compute_type    = "BUILD_GENERAL1_SMALL"
        image           = "aws/codebuild/amazonlinux2-x86_64-standard:2.0"
        type            = "LINUX_CONTAINER"
        privileged_mode = true

        environment_variable {
        name  = "AWS_DEFAULT_REGION"
        value = var.region_name
        }
        environment_variable {
        name  = "AWS_ACCOUNT_NUMBER"
        value = var.aws_account_number
        }
        environment_variable {
        name  = "ECR_REPO_NAME"
        value = var.aws_ecr_repo_name
        }
        environment_variable {
        name  = "ECR_IMAGE_NAME"
        value = var.ecr_image_name
        }
        environment_variable {
        name  = "ECR_CONTAINER_NAME"
        value = var.app_container_name
        }
    }
    artifacts {
        type = "CODEPIPELINE"
     }
     source {
         type = "CODEPIPELINE"
         buildspec = "buildspec.yml"
         //repository = var.code_commit_repo_name
     }
}