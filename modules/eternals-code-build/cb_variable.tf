
variable code_build_project_name {
}
variable service_role_code_build {
}
variable code_commit_repo_name {
}
variable region_name {
}
variable ecr_image_name {
}
variable aws_account_number {
}
variable aws_ecr_repo_name {
}

variable app_container_name {
}